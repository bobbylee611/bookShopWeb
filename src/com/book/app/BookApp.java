package com.book.app;

import java.util.ArrayList;
import java.util.List;

import com.book.model.Book;

public class BookApp {

	private void initApp() {
		Book book1 = new Book(1, "978-1484218013-1", "Java Design Patterns-1", 23.21, "Learn how to implement design patterns in Java-1");
		Book book2 = new Book(2, "978-1484218013-2", "Java Design Patterns-2", 23.22, "Learn how to implement design patterns in Java-2");
		Book book3 = new Book(3, "978-1484218013-3", "Java Design Patterns-3", 23.23, "Learn how to implement design patterns in Java-3");

		List<Book> bookList = new ArrayList<Book>();
		bookList.add(book1);
		bookList.add(book2);
		bookList.add(book3);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Book App started...");

		new BookApp().initApp();

	}

}
