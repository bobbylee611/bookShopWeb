package com.book.service;

import java.util.List;

import com.book.dao.BookDAOImpl;
import com.book.dao.IBookDAO;
import com.book.model.Book;

public class BookServiceImpl implements IBookService {

	IBookDAO bookDAO;

	public BookServiceImpl() {
		bookDAO = new BookDAOImpl();
	}

	@Override
	public Book get(long id) {
		return bookDAO.get(id);
	}

	@Override
	public void add(Book book) {
		this.bookDAO.add(book);

	}

	@Override
	public void update(Book updateBook) {
		this.bookDAO.update(updateBook);
	}

	@Override
	public void delete(long id) {
		this.bookDAO.delete(id);
	}

	@Override
	public List<Book> getBookList() {
		return this.bookDAO.getBookList();
	}
}
