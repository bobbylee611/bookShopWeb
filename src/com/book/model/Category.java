package com.book.model;

public enum Category {

	COMEDY("Comedy"), ARTS("Arts"), HISTORY("History");

	Category(String categoryName) {
		this.categoryName = categoryName;
	}

	private String categoryName;

	public String getCategoryName() {
		return categoryName;
	}
}
