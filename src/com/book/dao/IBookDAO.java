package com.book.dao;

import java.util.List;

import com.book.model.Book;

public interface IBookDAO {

	Book get(long id);

	void add(Book book);

	void update(Book updateBook);

	void delete(long id);

	List<Book> getBookList();

}
